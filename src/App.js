import React from 'react';
import './App.scss';
import {Dashboard} from "./views/Dashboard";
import { library } from '@fortawesome/fontawesome-svg-core'
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons'

library.add(faPlus, faTimes)

function App() {
    return (
        <header className="App-header">
            <Dashboard/>
        </header>
    );
}

export default App;
