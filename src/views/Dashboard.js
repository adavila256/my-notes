import React, {useState} from "react";
import {Button, Container, Nav, NavbarBrand} from "reactstrap";
import {TaskCreator} from "../components/TaskCreator";
import {TaskCard} from "../components/TaskCard";

export const Dashboard = () => {
    const [isOpen, setOpen] = useState(false);
    const [notesList, setNotesList] = useState([{title: 'To Do list', tasks: ['1', '2', '3']}])

    const toggleModal = (list) => {
        setOpen(!isOpen);
        setNotesList([...notesList, list])
    }

    return (
        <main className="">
            <Nav bar={"true"} expand="md" className="bg-white">
                <i className="far fa-list-alt"/>
                <NavbarBrand href="/" className="mr-auto">{`My todo list`}</NavbarBrand>
                <Button color={"primary"} onClick={toggleModal}>New</Button>
            </Nav>
            <Container fluid className="my-3">
                {notesList.map((d, i) => <TaskCard key={i} data={d}/>)}
            </Container>
            <TaskCreator open={isOpen} toggle={toggleModal}/>
        </main>
    )
}