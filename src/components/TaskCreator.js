import React, {useState} from "react";
import {Button, Input, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export const TaskCreator = ({open, toggle}) => {
    const [taskList, setTaskList] = useState({title: '', tasks: ['']});

    const closeModal = ()=>{
        toggle(taskList);
    }

    const addTask = () => {
        setTaskList({...taskList, tasks: [...taskList.tasks, '']});
    }

    const updateTitle = (val) => {
        setTaskList({...taskList, title: val});
    }

    const updateList = (value, index) => {
        taskList.tasks[index] = value;
        setTaskList(taskList);
    }

    const onKeyDown = (e) => {
        if (e.key === 'Enter') {
            addTask();
        }
    }

    const ListElem = ({data, index}) => {
        return (
            <div key={index} className="d-flex align-items-center">
                <Input type="checkbox" className="position-relative m-0 mr-2"/>
                <Input type="text" autoFocus placeholder={'Tarea'} className="border-0 shadow-none"
                       onChange={e => updateList(e.target.value, index)} defaultValue={data} onKeyDown={onKeyDown}/>
                <FontAwesomeIcon icon="times" color="gray" className="pointer" onClick={() => console.log(index)}/>
            </div>
        )
    }

    const isDisable = taskList.length === 1;

    return (
        <Modal isOpen={open} centered toggle={closeModal}>
            <ModalHeader className="py-2">
                <Input type="text" autoFocus placeholder={'Titulo'} className="border-0 shadow-none"/>
            </ModalHeader>
            <ModalBody>
                {taskList.tasks.map((e, i) => <ListElem key={i} data={e} index={i}/>)}
                <div className="mt-3">
                    <FontAwesomeIcon icon="plus" color="gray" className="mr-2 pointer"/>
                    <Button outline type="link" color="white" className="border-0 text-muted shadow-none"
                            disabled={isDisable} onClick={addTask}>Agregar tarea</Button>
                </div>
            </ModalBody>
            <ModalFooter>
                <Button outline size={'sm'} color={"white"} className="border-0 align-self-end"
                        onClick={() => console.log({taskList})}>
                    Cerrar
                </Button>
            </ModalFooter>
        </Modal>
    )
}