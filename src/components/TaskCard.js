import React from "react";
import {Card, CardBody, CardHeader, Input} from "reactstrap";

export const TaskCard = ({data}) => {

    const Tasks = () => {
        return (
            <>
                {data.tasks.map((d, i) => {
                    return (
                        <div key={i} className="d-flex align-items-center">
                            <Input type="checkbox" disabled className="position-relative m-0 mr-2"/>
                            <div className="text-truncate">{d}</div>
                        </div>
                    )
                })}
            </>
        )
    }
    return (
        <Card className="w-240 pointer">
            <CardHeader>{data.title}</CardHeader>
            <CardBody>
                <Tasks/>
            </CardBody>
        </Card>
    )
}